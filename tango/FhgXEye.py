############################################################################
# This file is part of LImA, a Library for Image Acquisition
#
# Copyright (C) : 2009-2011
# European Synchrotron Radiation Facility
# BP 220, Grenoble 38043
# FRANCE
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
############################################################################
#----------------------------------------------------------------------------
# The FhgXEye camera plugin TANGO interface
#----------------------------------------------------------------------------

import itertools
import PyTango

from Lima.Server import AttrHelper

from Lima import Core
from Lima import FhgXEye as CamMod

def grouper(n, iterable, padvalue=None):
    return zip(*[itertools.chain(iterable, itertools.repeat(padvalue, n-1))]*n)


class FhgXEye(PyTango.Device_4Impl):

#------------------------------------------------------------------
#    Static properties
#------------------------------------------------------------------
    _DetCamera = None
    """Reference to Lima fhgxeye camera binding"""
    _DetInterface = None
    """Reference to Lima fhgxeye interface binding"""

    _Detector = {
        'SIMULATOR': CamMod.Camera.Simulator,
        'DET5SENSORS': CamMod.Camera.Det5Sensors,
        'DET9SENSORS': CamMod.Camera.Det9Sensors,
	}

    _invDetector = {v: k for k, v in _Detector.items()}

    Core.DEB_CLASS(Core.DebModApplication, 'FhgXEye')

#------------------------------------------------------------------
#    Device constructor
#------------------------------------------------------------------
    def __init__(self,*args) :
        PyTango.Device_4Impl.__init__(self,*args)

        # Legacy code used by AttrHelper
        # switch to the new pyTango please
        self.__Detector = self._Detector

        # Load the properties
        self.get_device_properties(self.get_device_class())
        self.init_device()
        self.set_state(PyTango.DevState.ON)

#------------------------------------------------------------------
#    Device destructor
#------------------------------------------------------------------
    def delete_device(self):
        pass

#------------------------------------------------------------------
#    Device initialization
#------------------------------------------------------------------
    @Core.DEB_MEMBER_FUNCT
    def init_device(self):
        # Apply properties if any
        pass

    @Core.DEB_MEMBER_FUNCT
    def getAttrStringValueList(self, attr_name):
        return AttrHelper.get_attr_string_value_list(self, attr_name)

    def __getattr__(self, name):
        return AttrHelper.get_attr_4u(self, name, self._DetCamera)


class FhgXEyeClass(PyTango.DeviceClass):

    class_property_list = {}

    device_property_list = {
        }

    cmd_list = {
        'getAttrStringValueList':
        [[PyTango.DevString, "Attribute name"],
         [PyTango.DevVarStringArray, "Authorized String value list"]],
    }

    attr_list = {
        # FhgXEye detector
        'detector':
        [[PyTango.DevString,
          PyTango.SCALAR,
          PyTango.READ]],
        }

    def __init__(self,name) :
        PyTango.DeviceClass.__init__(self,name)
        self.set_type(name)

#----------------------------------------------------------------------------
# Plugins
#----------------------------------------------------------------------------

def get_control(
        _FhgXEye=FhgXEye,
        _Camera=CamMod.Camera,
        _Interface=CamMod.Interface,
        **kwargs
    ):
    """
    Called by LimaCCDs module to create Lima control object.

    In the fhgxeye scope, it also create the Lima camera, Lima interface
    and register them into the Tango fhgxeye class.

    Arguments:
        _FhgXEye: Tango class used to register Lima references
        _Camera: Lima class to create the camera instance
        _Interface: Lima class to create the interface instance
    """
    interface = _FhgXEye._DetInterface
    camera = _FhgXEye._DetCamera
    if interface is None:
        camera = _Camera()
        interface = _Interface(camera)
    control = Core.CtControl(interface)
    _FhgXEye._DetCamera = camera
    _FhgXEye._DetInterface = interface
    return control


def get_tango_specific_class_n_device():
    return FhgXEyeClass, FhgXEye
