//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2011
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043
// FRANCE
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "fhgxeye/FhgXEyeInterface.h"

using namespace lima;
using namespace lima::FhgXEye;
using namespace std;

/*******************************************************************
 * \brief Interface constructor
 *******************************************************************/

Interface::Interface(Camera &cam) : m_cam(cam), m_det_info(cam), m_sync(cam),
				    m_roi(cam)
{
  HwDetInfoCtrlObj *det_info = &m_det_info;
  m_cap_list.push_back(HwCap(det_info));

  HwBufferCtrlObj *buffer = cam.getBufferCtrlObj();
  m_cap_list.push_back(HwCap(buffer));

  HwSyncCtrlObj *sync = &m_sync;
  m_cap_list.push_back(HwCap(sync));

  HwRoiCtrlObj *roi = &m_roi;
  m_cap_list.push_back(HwCap(roi));
}

void Interface::getCapList(HwInterface::CapList &cap_list) const
{
  cap_list = m_cap_list;
}

void Interface::reset(ResetLevel reset_level)
{
  m_cam.reset();
}

void Interface::prepareAcq()
{
  m_cam.prepareAcq();
}

void Interface::startAcq()
{
  m_cam.startAcq();
}

void Interface::stopAcq()
{
  m_cam.stopAcq();
}

void Interface::getStatus(StatusType &status)
{
  status.set(m_cam.getStatus());
}

int Interface::getNbHwAcquiredFrames()
{
  return m_cam.getNbAcquiredFrames();
}
