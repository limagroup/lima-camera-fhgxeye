//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2011
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043
// FRANCE
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "fhgxeye/FhgXEyeDetInfoCtrlObj.h"

using namespace lima;
using namespace lima::FhgXEye;

void DetInfoCtrlObj::getMaxImageSize(Size &max_image_size)
{
  m_cam.getMaxImageSize(max_image_size);
}

void DetInfoCtrlObj::getDetectorImageSize(Size &det_image_size)
{
  FrameDim fdim;
  m_cam.getFrameDim(fdim);
  det_image_size = fdim.getSize();
}

void DetInfoCtrlObj::getDefImageType(ImageType &def_image_type)
{
  FrameDim fdim;
  m_cam.getFrameDim(fdim);
  def_image_type = fdim.getImageType();
}

void DetInfoCtrlObj::setCurrImageType(ImageType curr_image_type)
{
  ImageType image_type;
  getCurrImageType(image_type);
  if (curr_image_type != image_type)
    throw LIMA_HW_EXC(InvalidValue, "Invalid image type");
}

void DetInfoCtrlObj::getCurrImageType(ImageType &curr_image_type)
{
  FrameDim fdim;
  m_cam.getFrameDim(fdim);
  curr_image_type = fdim.getImageType();
}

void DetInfoCtrlObj::getPixelSize(double &x_size, double &y_size)
{
  x_size = y_size = 10e-6;
}

void DetInfoCtrlObj::getDetectorType(std::string &det_type)
{
  det_type = "FhgXEye";
}

void DetInfoCtrlObj::getDetectorModel(std::string &det_model)
{
  return m_cam.getDetectorModel(det_model);
}
