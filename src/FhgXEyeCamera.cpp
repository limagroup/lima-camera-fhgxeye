//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2011
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043
// FRANCE
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include <unistd.h>

#include <lima/Debug.h>

#include "fhgxeye/FhgXEyeCamera.h"

using namespace lima;
using namespace lima::FhgXEye;

Camera::AcqThread::AcqThread(Camera &cam) : m_cam(&cam)
{
  DEB_CONSTRUCTOR();

  m_acq_frame_nb = 0;
}

Camera::AcqThread::~AcqThread()
{
  DEB_DESTRUCTOR();
  abort();
}

void Camera::AcqThread::start()
{
  DEB_MEMBER_FUNCT();

  CmdThread::start();
  waitStatus(Ready);
}

void Camera::AcqThread::init()
{
  DEB_MEMBER_FUNCT();

  setStatus(Ready);
}

void Camera::AcqThread::execCmd(int cmd)
{
  DEB_MEMBER_FUNCT();

  switch (cmd) {
  case PrepareAcq:
    execPrepareAcq();
    break;
  case StartAcq:
    execStartAcq();
    break;
  case StopAcq:
  case Reset:
    setStatus(Ready);
    break;
  }
}

void Camera::AcqThread::execPrepareAcq()
{
  DEB_MEMBER_FUNCT();

  setStatus(Preparing);

  try {
    m_acq_frame_nb = 0;

    setStatus(Prepare);
  } catch (Exception &e) {
    DEB_ERROR() << e;
    setStatus(Fault);
  }
}

void Camera::AcqThread::execStartAcq()
{
  DEB_MEMBER_FUNCT();
  
  if (m_cam->m_trig_mode == ExtTrigSingle || m_cam->m_trig_mode == ExtTrigMult)
  {
    DEB_TRACE() << "Detector armed";
    setStatus(Armed);
  }
  else
    _exposure();
    
}

void Camera::AcqThread::_exposure()
{
  DEB_MEMBER_FUNCT();

  try {
    StdBufferCbMgr &buffer_mgr = m_cam->m_buffer_ctrl_obj.getBuffer();
    buffer_mgr.setStartTimestamp(Timestamp::now());

    int nb_frames = (m_cam->m_trig_mode == IntTrig || m_cam->m_trig_mode == ExtTrigSingle) ? m_cam->m_nb_frames
                                                                                             : m_acq_frame_nb + 1;
    int &frame_nb = m_acq_frame_nb;
    for (; (nb_frames == 0) || (frame_nb < nb_frames); frame_nb++) {
      if (getNextCmd() == StopAcq) {
        waitNextCmd();
        break;
      }
      double req_time;
      req_time = m_cam->m_exp_time;
      if (req_time > 1e-6) {
        setStatus(Exposure);
        usleep(long(req_time * 1e6));
      }

      setStatus(Readout);
      void *ptr_v = buffer_mgr.getFrameBufferPtr(frame_nb);
      unsigned char *ptr = reinterpret_cast<unsigned char *>(ptr_v);

      FrameDim frame_dim = buffer_mgr.getFrameDim();
      DEB_TRACE() << DEB_VAR1(frame_dim);

      // Get the next frame
      {
	Size size = frame_dim.getSize();
	unsigned short *ptr16 = reinterpret_cast<unsigned short *>(ptr_v);
	for (int y = 0; y < size.getHeight(); ++y)
	  for (int x = 0; x < size.getWidth(); ++x)
	    *ptr16++ = (x + y);
      }

      HwFrameInfoType frame_info;
      frame_info.acq_frame_nb = frame_nb;

      DEB_TRACE() << DEB_VAR1(frame_info);

      req_time = m_cam->m_lat_time;
      if (req_time > 1e-6) {
        setStatus(Latency);
        usleep(long(req_time * 1e6));
      }

      if (m_cam->m_trig_mode == IntTrigMult)
        // Make sure the detector is ready before calling newFrameReady
        setStatus(Ready);

      buffer_mgr.newFrameReady(frame_info);
    }
    setStatus(Ready);
  } catch (Exception &e) {
    DEB_ERROR() << e;
    setStatus(Fault);
  }
}


Camera::Camera() : m_thread(*this)
{
  DEB_CONSTRUCTOR();

  setDefaultProperties();

  m_thread.start();
  m_thread.waitStatus(AcqThread::Ready);
}

void Camera::setDefaultProperties()
{
  DEB_MEMBER_FUNCT();

  m_exp_time  = 1.0;
  m_lat_time  = 0.0;
  m_nb_frames = 1;
}

Camera::~Camera()
{
  DEB_DESTRUCTOR();
}

void Camera::getFrameDim(FrameDim &frame_dim)
{
  DEB_MEMBER_FUNCT();
  frame_dim = FrameDim(Size(1024, 1024), Bpp16);
  DEB_RETURN() << DEB_VAR1(frame_dim);
}

void Camera::getMaxImageSize(Size &max_image_size) const
{
  DEB_MEMBER_FUNCT();
  max_image_size = Size(1024, 1024);
  DEB_RETURN() << DEB_VAR1(max_image_size);
}

void Camera::getDetector(Detector &det) const
{
  DEB_MEMBER_FUNCT();
  det = m_det;
  DEB_RETURN() << DEB_VAR1(det);
}

void Camera::getDetectorModel(std::string &det_model) const
{
  DEB_MEMBER_FUNCT();
  static const char* DetectorModel[3] = {
    "Simulator",
    "Detector with 5 Sensors",
    "Detector with 9 Sensors"
  };

  det_model = DetectorModel[m_det];
  DEB_RETURN() << DEB_VAR1(det_model);
 
}

void Camera::setRoi(const Roi &roi)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(roi);
}

void Camera::getRoi(Roi &roi)
{
  DEB_MEMBER_FUNCT();
  Size max_size;
  getMaxImageSize(max_size);
  roi = Roi({0, 0}, max_size);
  DEB_RETURN() << DEB_VAR1(roi);
}

void Camera::checkRoi(const Roi &set_roi, Roi &hw_roi)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(set_roi);
  Size max_size;
  getMaxImageSize(max_size);
  hw_roi = Roi({0, 0}, max_size);
  DEB_RETURN() << DEB_VAR1(hw_roi);
}

void Camera::setNbFrames(int nb_frames)
{
  if (nb_frames < 0)
    throw LIMA_HW_EXC(InvalidValue, "Invalid nb of frames");

  m_nb_frames = nb_frames;
}

void Camera::getNbFrames(int &nb_frames)
{
  nb_frames = m_nb_frames;
}

void Camera::setExpTime(double exp_time)
{
  if (exp_time <= 0)
    throw LIMA_HW_EXC(InvalidValue, "Invalid exposure time");

  m_exp_time = exp_time;
}

void Camera::getExpTime(double &exp_time)
{
  exp_time = m_exp_time;
}

void Camera::setLatTime(double lat_time)
{
  if (lat_time < 0) throw LIMA_HW_EXC(InvalidValue, "Invalid latency time");

  m_lat_time = lat_time;
}

void Camera::getLatTime(double &lat_time)
{
  lat_time = m_lat_time;
}

void Camera::reset()
{
  if (m_thread.getStatus() == AcqThread::Fault) {
    m_thread.sendCmd(AcqThread::Reset);
    m_thread.waitStatus(AcqThread::Ready);
  } else {
    stopAcq();
  }

  setDefaultProperties();
}

HwInterface::StatusType::Basic Camera::getStatus()
{
  DEB_MEMBER_FUNCT();

  int thread_status = m_thread.getStatus();
  switch (thread_status) {
  case AcqThread::Ready:
  case AcqThread::Prepare:
  case AcqThread::Armed:
    return HwInterface::StatusType::Ready;
  case AcqThread::Exposure:
    return HwInterface::StatusType::Exposure;
  case AcqThread::Readout:
    return HwInterface::StatusType::Readout;
  case AcqThread::Latency:
    return HwInterface::StatusType::Latency;
  case AcqThread::Fault:
    return HwInterface::StatusType::Fault;
  default:
    THROW_HW_ERROR(Error) << "Invalid thread status";
  }
}

void Camera::prepareAcq()
{
  DEB_MEMBER_FUNCT();

  switch (m_thread.getStatus()) {
  case AcqThread::Prepare:
  case AcqThread::Ready:
  case AcqThread::Fault:
    break;
  default:
    THROW_HW_ERROR(Error) << "Camera not in Ready/Fault/Prepare status";
  }

  m_thread.sendCmd(AcqThread::PrepareAcq);
  m_thread.waitStatus(AcqThread::Preparing);
  if (m_thread.waitNotStatus(AcqThread::Preparing) != AcqThread::Prepare)
     THROW_HW_ERROR(Error) << "Prepare failed";
}

void Camera::startAcq()
{
  DEB_MEMBER_FUNCT();

  int thread_status = m_thread.getStatus();
  if ((thread_status != AcqThread::Prepare) &&
      (thread_status != AcqThread::Ready) &&
      (thread_status != AcqThread::Armed))
    THROW_HW_ERROR(Error) << "Camera not Prepared nor Armed (Multi Trigger)";

  m_buffer_ctrl_obj.getBuffer().setStartTimestamp(Timestamp::now());

  m_thread.sendCmd(AcqThread::StartAcq);
  if (m_thread.waitNotStatus(thread_status) == AcqThread::Fault)
    THROW_HW_ERROR(Error) << "StartAcq failed";
}

void Camera::stopAcq()
{
  DEB_MEMBER_FUNCT();

  switch (m_thread.getStatus()) {
  case AcqThread::Exposure:
  case AcqThread::Readout:
  case AcqThread::Latency:
  case AcqThread::Armed:
    m_thread.sendCmd(AcqThread::StopAcq);
    m_thread.waitStatus(AcqThread::Ready);
    break;

  case AcqThread::Fault:
    THROW_HW_ERROR(Error) << "Camera in Fault state";
  }
}

int Camera::getNbAcquiredFrames()
{
  return m_thread.m_acq_frame_nb;
}

std::ostream &lima::FhgXEye::operator<<(std::ostream &os, Camera &cam)
{
  std::string status;
  switch (cam.getStatus()) {
  case HwInterface::StatusType::Ready:
    status = "Ready";
    break;
  case HwInterface::StatusType::Exposure:
    status = "Exposure";
    break;
  case HwInterface::StatusType::Readout:
    status = "Readout";
    break;
  case HwInterface::StatusType::Latency:
    status = "Latency";
    break;
  default:
    status = "Unknown";
  }
  os << "<status=" << status << ">";
  return os;
}

std::ostream &lima::FhgXEye::operator<<(std::ostream &os, Camera::Detector det)
{
  std::string s;
  switch (det) {
  case Camera::Simulator:
    s = "Simulator";
    break;
  case Camera::Det5Sensors:
    s = "Det5Sensors";
    break;
  case Camera::Det9Sensors:
    s = "Det9Sensors";
    break;
  default:
    s = "Unknown";
  }
  os << s;
  return os;
}
