FhgXEye Tango device
======================

This is the reference documentation of the FhgXEye Tango device.

you can also find some useful information about the camera models/prerequisite/installation/configuration/compilation in the :ref:`FhgXEye camera plugin <camera-fhgxeye>` section.

Properties
----------

=============== =============== =============== =========================================================================
Property name	Mandatory	Default value	Description
=============== =============== =============== =========================================================================
=============== =============== =============== =========================================================================

Attributes
----------
======================= ======= ======================= ======================================================================
Attribute name		RW	Type			Description
======================= ======= ======================= ======================================================================
detector		ro	DevString		The kind of detector
======================= ======= ======================= ======================================================================

Commands
--------

=======================	=============== =======================	===========================================
Command name		Arg. in		Arg. out		Description
=======================	=============== =======================	===========================================
Init			DevVoid 	DevVoid			Do not use
State			DevVoid		DevLong			Return the device state
Status			DevVoid		DevString		Return the device state as a string
getAttrStringValueList	DevString:	DevVarStringArray:	Return the authorized string value list for
			Attribute name	String value list	a given attribute name
=======================	=============== =======================	===========================================


Database description
''''''''''''''''''''

This is a representation of the Tango database content.

.. code-block:: yaml

   personal_name: my_fhgxeye
   server: LimaCCDs
   device:
   - class: MyFhgXEye
     tango_name: id00/myfhgxeye/my_fhgxeye
     properties: {}
   - class: LimaCCDs
     properties:
       LimaCameraType: MyFhgXEye  # Ask to use your custom camera

Start the tango device
''''''''''''''''''''''

.. code-block:: sh

   LimaCCDs my_fhgxeye
