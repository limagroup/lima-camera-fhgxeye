//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2011
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043
// FRANCE
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#pragma once

#if !defined(FHGXEYE_CAMERA_H)
#define FHGXEYE_CAMERA_H

#include <ostream>

#include <lima/HwInterface.h>
#include <lima/HwBufferMgr.h>
#include <lima/ThreadUtils.h>
#include <lima/SizeUtils.h>
#include <processlib/Data.h>

#include <fhgxeye_export.h>

namespace lima {

namespace FhgXEye {

class FHGXEYE_EXPORT Camera {
  DEB_CLASS_NAMESPC(DebModCamera, "Camera", "FhgXEye");

public:
  enum Detector { Simulator, Det5Sensors, Det9Sensors };

  Camera();
  ~Camera();

  HwBufferCtrlObj *getBufferCtrlObj() { return &m_buffer_ctrl_obj; }

  /// Return the detector type & model
  void getDetector(Detector &det) const;
  void getDetectorModel(std::string &det_model) const;

  void prepareAcq();
  void startAcq();
  void stopAcq();

  void setRoi(const Roi &roi);
  void getRoi(Roi &roi);
  void checkRoi(const Roi &set_roi, Roi &hw_roi);

  void setNbFrames(int nb_frames);
  void getNbFrames(int &nb_frames);

  void setExpTime(double exp_time);
  void getExpTime(double &exp_time);

  void setLatTime(double lat_time);
  void getLatTime(double &lat_time);

  void setTrigMode(TrigMode trig_mode) { m_trig_mode = trig_mode; };
  void getTrigMode(TrigMode &trig_mode) { trig_mode = m_trig_mode; };

  void getFrameDim(FrameDim &frame_dim);
  void getMaxImageSize(Size &max_image_size) const;

  HwInterface::StatusType::Basic getStatus();
  int getNbAcquiredFrames();

  void reset();

private:
  class AcqThread : public CmdThread {
    DEB_CLASS_NAMESPC(DebModCamera, "Camera", "AcqThread");

  public:
    enum { // Status
      Ready = MaxThreadStatus,
      Preparing,
      Prepare,
      Exposure,
      Readout,
      Latency,
      Armed,
      Fault,
    };

    enum { // Cmd
      PrepareAcq = MaxThreadCmd,
      StartAcq,
      StopAcq,
      Reset,
    };

    AcqThread(Camera &cam);
    ~AcqThread();

    virtual void start();

    int m_acq_frame_nb;

  protected:
    virtual void init();
    virtual void execCmd(int cmd);

  private:
    void execPrepareAcq();
    void execStartAcq();
    void _exposure();
    Camera *m_cam;
  };
  friend class AcqThread;

  void setDefaultProperties();

  double m_exp_time;
  double m_lat_time;
  int m_nb_frames;

  TrigMode m_trig_mode;

  SoftBufferCtrlObj m_buffer_ctrl_obj;

  Detector m_det;                 //<! The detector type

  AcqThread m_thread;
};

FHGXEYE_EXPORT std::ostream &operator<<(std::ostream &os, Camera &cam);

FHGXEYE_EXPORT std::ostream &operator<<(std::ostream &os, Camera::Detector det);

} // namespace FhgXEye

} // namespace lima

#endif // !defined(FHGXEYE_CAMERA_H)
